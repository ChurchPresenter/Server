# XALPresenter - Server

## Syntax rules

For code:

- namespaces are written in snake case

For files & directories:

- directories are written in snake case;
- files are written in pascal case.

## Structure

- database
- gui
- network
- plugin_filesystem
- plugin_presenter
- plugin_setup
- plugin_users
- plugin_physicaldisplay
- plugin_webdisplay
- plugins_manager
- resources_manager


#ifndef RESOURCES_MANAGER_TYPES_DBMANAGERTYPE_HPP
#define RESOURCES_MANAGER_TYPES_DBMANAGERTYPE_HPP

// Own headers
#include "AbstractType.hpp"
#include <database/DBManager.hpp>

struct ResourceManager_DBManagerType : ResourceManager_AbstractType {
    DBManager dbManager;

    ResourceManager_DBManagerType()
        : dbManager { }
    {}

    DBManager& get() {
        return dbManager;
    }

    static ResourceManager_DBManagerType* create() {
        ResourceManager_DBManagerType* type = new ResourceManager_DBManagerType();
        return type;
    }
};

#endif // RESOURCES_MANAGER_TYPES_DBMANAGERTYPE_HPP

#ifndef RESOURCES_MANAGER_TYPES_ABSTRACTTYPE_HPP
#define RESOURCES_MANAGER_TYPES_ABSTRACTTYPE_HPP

struct ResourceManager_AbstractType {
    explicit ResourceManager_AbstractType() = default;
    ResourceManager_AbstractType(const ResourceManager_AbstractType&) = delete;
    virtual ResourceManager_AbstractType& operator=(const ResourceManager_AbstractType&) = delete;
    virtual ~ResourceManager_AbstractType() = default;
};

#endif // RESOURCES_MANAGER_TYPES_ABSTRACTTYPE_HPP

#ifndef RESOURCES_MANAGER_TYPES_PLUGINSMANAGERTYPE_HPP
#define RESOURCES_MANAGER_TYPES_PLUGINSMANAGERTYPE_HPP

// Own headers
#include "AbstractType.hpp"
#include <plugins_manager/PluginsManager.hpp>

struct ResourceManager_PluginsManagerType : ResourceManager_AbstractType {
    PluginsManager pluginsManager;

    ResourceManager_PluginsManagerType(std::shared_ptr<ResourceManager<QString>> resourceManager)
        : pluginsManager { resourceManager }
    {}

    PluginsManager& get() {
        return pluginsManager;
    }

    static ResourceManager_PluginsManagerType* create(std::shared_ptr<ResourceManager<QString>> resourceManager) {
        ResourceManager_PluginsManagerType* type = new ResourceManager_PluginsManagerType(resourceManager);
        return type;
    }
};

#endif // RESOURCES_MANAGER_TYPES_PLUGINSMANAGERTYPE_HPP

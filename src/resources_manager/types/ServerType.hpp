#ifndef RESOURCES_MANAGER_TYPES_SERVERTYPE_HPP
#define RESOURCES_MANAGER_TYPES_SERVERTYPE_HPP

// Own headers
#include "AbstractType.hpp"
#include <network/Server.hpp>

struct ResourceManager_ServerType : ResourceManager_AbstractType {
    Server server;

    ResourceManager_ServerType(std::shared_ptr<ResourceManager<QString>> resourceManager, QObject *parent = nullptr)
        : server { resourceManager, parent }
    {}

    Server& get() {
        return server;
    }

    static ResourceManager_ServerType* create(std::shared_ptr<ResourceManager<QString>> resourceManager, QObject *parent = nullptr) {
        ResourceManager_ServerType* type = new ResourceManager_ServerType(resourceManager, parent);
        return type;
    }
};

#endif // RESOURCES_MANAGER_TYPES_SERVERTYPE_HPP

#ifndef RESOURCES_MANAGER_RESOURCEMANAGER_HPP
#define RESOURCES_MANAGER_RESOURCEMANAGER_HPP

// Qt headers
#include <QString>
#include <QHash>
#include <QScopedPointer>
#include <QSharedPointer>
#include <memory>

// Own headers
#include "types/AbstractType.hpp"

template <typename R>
class ResourceManager
{
public:
    ResourceManager() = default;

    void set(const R& name, ResourceManager_AbstractType* object);

    template <typename T>
    T* get(const R& name);

private:
    QHash<R, QSharedPointer<ResourceManager_AbstractType>> m_objectsMap;
};

template <typename R>
void ResourceManager<R>::set(const R& name, ResourceManager_AbstractType* object) {
    m_objectsMap.insert(name, QSharedPointer<ResourceManager_AbstractType>(object));
}

template <typename R>
template <typename T>
T* ResourceManager<R>::get(const R& name) {
    auto it = m_objectsMap.find(name);

    if (it == m_objectsMap.end()) throw std::invalid_argument("The item doesn't exists");

    return static_cast<T*>(it.value().get());
}
#endif // RESOURCES_MANAGER_RESOURCEMANAGER_HPP

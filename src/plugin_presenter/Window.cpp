#include "Window.hpp"

// Qt headers
#include <QVBoxLayout>
#include <QFont>
#include <QFontMetrics>

#include <QDebug>

Window::Window(QWidget *parent) : QWidget(parent)
{
    m_text->setWordWrap(true);
    m_text->setAlignment(Qt::AlignCenter);

    auto mainLayout = new QVBoxLayout;
    mainLayout->addWidget(m_text);

    this->setLayout(mainLayout);
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
}

void Window::setText(const QString& text) {
    m_text->setText(text);

    if (m_autoTextSize) autoTextSize();
}

void Window::setAutoTextSize(bool autoTextSize) {
    m_autoTextSize = autoTextSize;

    if (autoTextSize) {
        this->autoTextSize();
    }
}

void Window::setStyle(const WindowStyle& style) {
    if (m_style.backgroundColor != style.backgroundColor) {
        this->setStyleSheet("background-color: " + style.backgroundColor + ";");
    }

    if (m_style.fontColor != style.fontColor || m_style.fontFamily != style.fontFamily) {
        createTextStyle(style);
    }

    if (m_style.fontFamily != style.fontFamily) {
        autoTextSize();
    }

    m_style = style;
}

void Window::autoTextSize() {
    QFont f = m_text->font(); //Get label font

    QFontMetrics metrics(f);
    QSize size = metrics.size(0, m_text->text()); //Get size of text
    float factorw = this->width() / static_cast<float>(size.width()) * 0.9; //Get the width factor
    float factorh = this->height() / static_cast<float>(size.height()) * 0.9; //Get the height factor

    qDebug() << size << m_text->geometry();

    float factor = qMin(factorw, factorh); //To fit contents in the screen select as factor
    //the minimum factor between width and height

    f.setPointSizeF(f.pointSizeF() * factor); //Set font size
    m_text->setFont(f); //Set the adjusted font to the label
}

void Window::createTextStyle(const WindowStyle& style) {
    m_text->setStyleSheet("color: " + style.fontColor + "; font-family: \"" + style.fontFamily + "\";");
}

#ifndef PLUGIN_PRESENTER_SOCKET_DATA_MESSAGEACTION_HPP
#define PLUGIN_PRESENTER_SOCKET_DATA_MESSAGEACTION_HPP

namespace PresenterPluginNS {
    static const QString MESSAGEACTION_GET_SCREENS = "get_screens";
    static const QString MESSAGEACTION_SHOW_SCREENS_INFO = "show_screens_info";
    static const QString MESSAGEACTION_CHANGE_SETTINGS = "change_settings";
    static const QString MESSAGEACTION_CHANGE_SCREEN_SETTINGS = "change_screen_settings";
    static const QString MESSAGEACTION_PREVIEW = "preview";
    static const QString MESSAGEACTION_GET_FONTS = "get_fonts";
    static const QString MESSAGEACTION_LOAD_FILE = "load_file";
    static const QString MESSAGEACTION_SAVE_FILE = "save_file";
}

#endif // PLUGIN_PRESENTER_SOCKET_DATA_MESSAGEACTION_HPP

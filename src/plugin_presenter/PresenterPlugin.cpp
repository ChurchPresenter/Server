#include "PresenterPlugin.hpp"

// Qt headers
#include <QGuiApplication>
#include <QScreen>
#include <QFile>
#include <QJsonArray>
#include <QFontDatabase>

#include "socket_data/MessageAction.hpp"
#include <network/utils/FormatMessageUtils.hpp>
#include <resources_manager\ResourceManager.hpp>
#include <plugins_manager/utils/getPluginFromRM.hpp>
#include <plugins_manager/PluginsManager.hpp>
#include <plugin_users/UsersPlugin.hpp>

#include <QDebug>

PresenterPlugin::PresenterPlugin(std::shared_ptr<ResourceManager<QString>> resourceManager)
    : Plugin(resourceManager)
{
    auto screens = QGuiApplication::screens();
    for (auto screen : screens) {
        qDebug() << screen << screen->serialNumber() << screen->model();

        Screen screenStruct;
        screenStruct.geometry = screen->geometry();

        m_screens.insert(screen->model() + screen->serialNumber(), screenStruct);
    }

    readSettings();
}

QString PresenterPlugin::execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client) {
    qDebug() << "[PresenterPlugin][execCommand]" << "clientID('" + clientID + "')" << "messageID('" + messageID + "')" << "message='" << message << "'";
    auto a = getUsersPluginFromRM();
    if (!a->checkUserLogged(client)) {
        return formatErrorMessage("Not logged in", messageID);
    }

    int action = message["action"].toInt(-1);

    if (action == PresenterPluginNS::MESSAGEACTION_GET_SCREENS) return actionGetScreens(messageID);
    else if (action == PresenterPluginNS::MESSAGEACTION_SHOW_SCREENS_INFO) {
        auto result = actionShowScreensInfo(messageID);
        return formatSuccessMessage(result, messageID);
    } 
    else if (action == PresenterPluginNS::MESSAGEACTION_CHANGE_SETTINGS) return actionChangeSettings(message, messageID);
    else if (action == PresenterPluginNS::MESSAGEACTION_CHANGE_SCREEN_SETTINGS) {
        auto result = actionChangeScreenSettings(message, messageID);
        return result;
    }
    else if (action == PresenterPluginNS::MESSAGEACTION_PREVIEW) {
        if (message.find("screenType") == message.end()) {
            return formatErrorMessage("[DEVBUG]Nu s-o trimis screenType", messageID);
        }

        int screenType = message["screenType"].toInt(-1);

        if (screenType != ScreenType::PRIMARY_SCREEN && screenType != ScreenType::SECONDARY_SCREEN) {
            return formatErrorMessage("[DEVBUG]screenType is wrong", messageID);
        }

        auto result = actionPreview(message["screenType"].toInt(), messageID);
        return result;
    }
    else if (action == PresenterPluginNS::MESSAGEACTION_GET_FONTS) return actionGetFonts(messageID);
    else                                                         return formatErrorMessage("Unrecognized action", messageID);
}

QString PresenterPlugin::actionGetScreens(const QString &id) {
    QJsonObject result;

    QJsonArray screensJson;
    for (auto screenName : m_screens.keys()) {
        auto rect = m_screens[screenName].geometry;

        QJsonObject geometry;
        geometry["x"] = rect.x();
        geometry["y"] = rect.y();
        geometry["width"] = rect.width();
        geometry["height"] = rect.height();

        QJsonObject screen;
        screen["name"] = screenName;
        screen["geometry"] = geometry;
        screen["screenType"] = m_screens[screenName].screenType;

        screensJson.append(screen);
    }

    result["screens"] = screensJson;

//    result["primaryScreenStyling"] = windowStyleToJson(m_globalPrimaryScreenSettings);
//    result["secondaryScreenStyling"] = windowStyleToJson(m_globalSecondaryScreenSettings);

    return formatSuccessMessage(result, id);
}

QJsonObject PresenterPlugin::actionShowScreensInfo(const QString& id) {
    for (auto screenName : m_screens.keys()) {
        createInfoScreen(screenName);
    }

    QJsonObject result;
    return result;
}

void PresenterPlugin::createInfoScreen(const QString& screenName) {
    m_screens[screenName].window = new Window;
    m_screens[screenName].window->setGeometry(m_screens[screenName].geometry);
    m_screens[screenName].window->setText(screenName + "\n" + QString::number(m_screens[screenName].geometry.x()) + "x" + QString::number(m_screens[screenName].geometry.y()) + " " + QString::number(m_screens[screenName].geometry.width()) + "x" + QString::number(m_screens[screenName].geometry.height()));
    m_screens[screenName].window->show();
}

QString PresenterPlugin::actionPreview(int screenType, const QString& id) {
    QList<QString>* list;
    if (screenType == ScreenType::PRIMARY_SCREEN) {
        list = &m_primaryScreens;
    } else {
        list = &m_secondaryScreens;
    }

    QJsonObject result;
    if (list->size() == 0) {
        result["screenType"] = screenType;
        return formatErrorMessage("No screens available", result, id);
    }

    bool preview;
    if (screenType == ScreenType::PRIMARY_SCREEN) {
        m_isPrimaryScreenPreview = !m_isPrimaryScreenPreview;
        preview = m_isPrimaryScreenPreview;
    } else {
        m_isSecondaryScreenPreview = !m_isSecondaryScreenPreview;
        preview = m_isSecondaryScreenPreview;
    }

    if (preview) {
        if (screenType == ScreenType::PRIMARY_SCREEN) {
            m_previewPrimaryScreenSettings = m_globalPrimaryScreenSettings;
        } else {
            m_previewSecondaryScreenSettings = m_globalSecondaryScreenSettings;
        }
    }

    for (auto screenName : *list) {
        if (preview) {
            createInfoScreen(screenName);
        } else {
            m_screens[screenName].window->close();
        }
    }

    return formatSuccessMessage(result, id);
}

QString PresenterPlugin::actionChangeScreenSettings(QJsonObject& data, const QString& id) {
    if (data.find("screenType") == data.end()) {
        return formatErrorMessage("[DEVBUG]Nu s-o trimis screenType", id);
    }

    int screenType = data["screenType"].toInt(-1);

    if (screenType != ScreenType::PRIMARY_SCREEN && screenType != ScreenType::SECONDARY_SCREEN) {
        return formatErrorMessage("[DEVBUG]screenType is wrong", id);
    }

    if ((screenType == ScreenType::PRIMARY_SCREEN && !m_isPrimaryScreenPreview) || (screenType == ScreenType::SECONDARY_SCREEN && !m_isSecondaryScreenPreview)) {
        return formatErrorMessage("[DEVBUG]no screen active", id);
    }

    QList<QString>* list;
    WindowStyle currentStyle;
    if (screenType == ScreenType::PRIMARY_SCREEN) {
        list = &m_primaryScreens;
        currentStyle = m_previewPrimaryScreenSettings;
    } else {
        list = &m_secondaryScreens;
        currentStyle = m_previewSecondaryScreenSettings;
    }

    for (auto screenName : *list) {
        currentStyle.backgroundColor = data["backgroundColor"].toString();
        currentStyle.fontColor = data["fontColor"].toString();
        currentStyle.fontFamily = data["fontFamily"].toString();

        m_screens[screenName].window->setStyle(currentStyle);
    }

    return formatSuccessMessage(id);
}

QString PresenterPlugin::actionGetFonts(const QString& id) {
    QFontDatabase fontDabase;

    QJsonArray fonts;
    for (auto fontFamily : fontDabase.families()) {
        fonts.append(fontFamily);
    }

    QJsonObject result;
    result["fonts"] = fonts;

    return formatSuccessMessage(result, id);
}

QString PresenterPlugin::actionChangeSettings(QJsonObject& data, const QString& id) {
    QJsonObject settings;

    //= Screens lists ==================================================================================================
    QString screensListKeys[] = { "primaryScreens", "secondaryScreens" };

    for (auto key : screensListKeys) {
        if (data.find(key) == data.end()) {
            return formatErrorMessage(key + " was not sent", id);
        } else if (!data[key].isArray()) {
            return formatErrorMessage(key + " must be an array", id);
        }

        qDebug() << "actionChangeSettings()" << key << data[key];

        settings[key] = data[key];
    }
    //==================================================================================================================

    //= Screen settings ================================================================================================
    QString screenSettingsKeys[] = { "primaryScreen", "secondaryScreen" };
    for (auto key : screenSettingsKeys) {
        auto screenData = data[key].toObject();
        QJsonObject screenSettings;

        for (auto field : SCREENSETTINGS_KEYS) {
            if (screenData.find(field) != screenData.end()) {
                screenSettings[field] = screenData[field];
            } else {
                // Warning message
            }
        }

        settings[key] = screenSettings;
    }
    //==================================================================================================================

    QFile settingsFile(SETTINGS_FILENAME);
    settingsFile.open(QIODevice::WriteOnly);
    settingsFile.write(QJsonDocument(settings).toJson(QJsonDocument::Compact));

    readSettings(&settings);

    return formatSuccessMessage(id);
}

void PresenterPlugin::readSettings(QJsonObject* data) {
    if (data == nullptr) {
        QFile settingsFile(SETTINGS_FILENAME);

        if (!settingsFile.exists()) return;

        settingsFile.open(QIODevice::ReadOnly);

        data = new QJsonObject(QJsonDocument::fromJson(settingsFile.readAll()).object());
    }

    //= Screens lists ==================================================================================================
    m_primaryScreens.clear();
    m_secondaryScreens.clear();

    QString screensListKeys[] = { "primaryScreens", "secondaryScreens" };
    for (auto key : screensListKeys) {
        for (auto screenNamesJson : data->value(key).toArray()) {
            auto name = screenNamesJson.toString();
            auto it = m_screens.find(name);

            if (it != m_screens.end()) {
                if (key == "primaryScreens") {
                    m_primaryScreens.append(name);
                } else {
                    m_secondaryScreens.append(name);
                }
            }
        }
    }
    //==================================================================================================================

    //= Screen settings ================================================================================================
    QString screenSettingsKeys[] = { "primaryScreen", "secondaryScreen" };
    for (auto key : screenSettingsKeys) {
        auto screenData = data->value(key).toObject();

        WindowStyle* windowStyle;
        if (key == "primaryScreen") windowStyle = &m_globalPrimaryScreenSettings;
        else if (key == "secondaryScreen") windowStyle = &m_globalSecondaryScreenSettings;

        for (auto field : SCREENSETTINGS_KEYS) {
            if (screenData.find(field) != screenData.end()) {
                SCREENSETTINGS_APPLY[field](*windowStyle, screenData[field]);
            } else {
                // Warning message
            }
        }
    }

    qDebug() << "primaryScreen" << m_globalPrimaryScreenSettings.backgroundColor;
    //==================================================================================================================
}

QJsonObject PresenterPlugin::windowStyleToJson(const WindowStyle& style) {
    QJsonObject result;
    result["backgroundColor"] = style.backgroundColor;
    result["fontColor"] = style.fontColor;
    result["fontFamily"] = style.fontFamily;

    qDebug() << "windowStyleToJson()" << result;
    return result;
}

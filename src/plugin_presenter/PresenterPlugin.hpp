#ifndef PLUGIN_PRESENTER_PRESENTERPLUGIN_HPP
#define PLUGIN_PRESENTER_PRESENTERPLUGIN_HPP

// Qt headers
#include <QWebSocket>
#include <QRect>
#include <QPointer>
#include <QHash>

// C++ headers
#include <memory>
#include <functional>

// Own headers
#include "Window.hpp"
#include "data/ScreenType.hpp"
#include <plugins_manager/Plugin.hpp>

const QStringList SCREENSETTINGS_KEYS = { "backgroundColor", "fontColor", "fontFamily" };
const QHash<QString, std::function<void(WindowStyle& style, QJsonValueRef value)>> SCREENSETTINGS_APPLY = {
    {"backgroundColor", [](WindowStyle& style, QJsonValueRef value){ style.backgroundColor = value.toString(); }},
    {"fontColor", [](WindowStyle& style, QJsonValueRef value){ style.fontColor = value.toString(); }},
    {"fontFamily", [](WindowStyle& style, QJsonValueRef value){ style.fontFamily = value.toString(); }}
};

struct Screen {
    QRect geometry;
    QPointer<Window> window = nullptr;

    int screenType = ScreenType::NONE;
};

class PresenterPlugin : public Plugin
{
public:
    PresenterPlugin(std::shared_ptr<ResourceManager<QString>> resourceManager);
    virtual ~PresenterPlugin() = default;

    QString execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client);

private:
    /**
     * @brief actionGetGlobalSettings
     * @param data
     * @param id
     * @return
     */
    QString     actionGetScreens(const QString& id);

    /**
     * @brief actionShowScreensInfo
     * @param id
     * @return
     */
    QJsonObject actionShowScreensInfo(const QString& id);
    QString     actionPreview(int screenType, const QString& id);
    QString     actionChangeScreenSettings(QJsonObject& data, const QString& id);
    QString     actionGetFonts(const QString& id);
    QString     actionChangeSettings(QJsonObject& data, const QString& id);
    QString     actionLoadFile(QJsonObject& data, const QString& id);
    QString     actionSaveFile(QJsonObject& data, const QString& id);

    void createInfoScreen(const QString& screenName);

    void readSettings(QJsonObject* data = nullptr);

    QJsonObject windowStyleToJson(const WindowStyle& style);

private:
    const QString SETTINGS_FILENAME = "presenter.json";

private:
    QHash<QString, Screen> m_screens;

    QList<QString> m_primaryScreens;
    QList<QString> m_secondaryScreens;

    WindowStyle m_globalPrimaryScreenSettings;
    WindowStyle m_globalSecondaryScreenSettings;

    WindowStyle m_previewPrimaryScreenSettings;
    WindowStyle m_previewSecondaryScreenSettings;

    bool m_isPrimaryScreenPreview = false;
    bool m_isSecondaryScreenPreview = false;

    bool m_showingScreensInfo = false;
};

#endif // PLUGIN_PRESENTER_PRESENTERPLUGIN_HPP

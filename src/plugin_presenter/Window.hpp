#ifndef PLUGIN_PRESENTER_WINDOW_HPP
#define PLUGIN_PRESENTER_WINDOW_HPP

// Qt headers
#include <QWidget>
#include <QLabel>
#include <QPointer>

struct WindowStyle {
    QString backgroundColor;
    QString fontColor;
    QString fontFamily;
};

class Window : public QWidget
{
    Q_OBJECT
public:
    explicit Window(QWidget *parent = nullptr);

    void setText(const QString& text);

    void setStyle(const WindowStyle& style);

    void setAutoTextSize(bool autoTextSize);
signals:

public slots:

private:
    void autoTextSize();

    void createTextStyle(const WindowStyle& style);

private:
    QPointer<QLabel> m_text = new QLabel;

    WindowStyle m_style;

    bool m_autoTextSize = true;
};

#endif // PLUGIN_PRESENTER_WINDOW_HPP

// Qt headers
#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QStringList>

// C++ headers
#include <memory>

// Own headers
#include <network/Server.hpp>
#include <database/DBManager.hpp>
#include <plugins_manager/PluginsManager.hpp>
#include <plugins_manager/Plugin.hpp>
// #include <windows/MainWindow.hpp>
#include <resources_manager/ResourceManager.hpp>
#include <resources_manager/ResourceManagerKeys.hpp>
#include <resources_manager/types/PluginsManagerType.hpp>
#include <resources_manager/types/ServerType.hpp>
#include <resources_manager/types/DBManagerType.hpp>

#include <QDebug>

int main(int argc, char *argv[])
{
    qDebug() << "Starting XALPresenter...";

    QApplication app(argc, argv);

    // Create the global resource manager which will hold the main objects as database, server, plugins manager
    std::shared_ptr<ResourceManager<QString>> resourceManager = std::make_shared<ResourceManager<QString>>();

    // Create the database manager
    auto dbManagerHLD = ResourceManager_DBManagerType::create();
    resourceManager->set(RESOURCEMANAGER_DATABASE, dbManagerHLD);

    // Create the plugins manager
    auto pluginsManagerHLD = ResourceManager_PluginsManagerType::create(resourceManager);
    resourceManager->set(RESOURCEMANAGER_PLUGINSMANAGER, pluginsManagerHLD);

    // Add the plugins to the plugins manager
    // TODO: Find a way to add the plugins automatically from DLLs
    pluginsManagerHLD->get().addPlugin(static_cast<uint>(PluginsManager::PluginKeys::PRESENTER));
    pluginsManagerHLD->get().addPlugin(static_cast<uint>(PluginsManager::PluginKeys::FILESYSTEM));
    pluginsManagerHLD->get().addPlugin(static_cast<uint>(PluginsManager::PluginKeys::USERS));

    // Create the server
    auto serverHLD = ResourceManager_ServerType::create(resourceManager);
    resourceManager->set(RESOURCEMANAGER_SERVER, serverHLD);

    qDebug() << "Server:" << serverHLD->get().run(3000);

    if (!dbManagerHLD->get().connect("data/db.sqlite")) {

    }

#ifdef GUI
    MainWindow w;
    w.show();
#endif

    return app.exec();
}

#include "Client.hpp"

// Qt headers
#include <QSharedPointer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonParseError>

// Own headers
#include "socket_data/MessageHeader.hpp"
#include "utils/FormatMessageUtils.hpp"
#include <plugins_manager/Plugin.hpp>
#include <plugin_users/socket_data/MessageHeader.hpp>

#include <QDebug>

const QString Client::ERROR_OBJECT_FIELD_NAME = "__errors";

Client::Client(QWebSocket* socket, QString clientID, PluginsManager* pluginsManager)
    : QObject { nullptr }
    , m_socket { socket }
    , m_clientID { clientID }
    , m_pluginsManager { pluginsManager }
{
    connect(m_socket, &QWebSocket::textMessageReceived, this, &Client::textMessageReceived);
}

void Client::textMessageReceived(const QString &message) {
    qDebug() << "[Network:Client:textMessageReceived][clientID:" + m_clientID + "]Received j\"" + message + "\"";
    auto data = parseMessage(message);

    if (data.find(ERROR_OBJECT_FIELD_NAME) == data.end()) {
        m_socket->sendTextMessage(formatErrorMessage("Wrong formatted message received"));
        return;
    }

    const QString command = data["header"].toString();
    const QString messageID = data["id"].toString();

    if (command == MESSAGEHEADER_USERS) {
        m_socket->sendTextMessage(m_pluginsManager->getPlugin(PluginsManager::PluginKeys::USERS)->execCommand(data, messageID, m_clientID, *this));
    } else {
        m_socket->sendTextMessage(formatErrorMessage("Unrecognized command", messageID));
    }

    // switch (command) {
    //     case static_cast<int>(MessageHeader::USERS):
            // m_socket->sendTextMessage(m_pluginsManager->getPlugin(PluginsManager::PluginKeys::USERS)->execCommand(data, messageID, m_clientID, *this));
            // break;
    //     case static_cast<int>(MessageHeader::PRESENTER):
    //         m_socket->sendTextMessage(m_pluginsManager->getPlugin(PluginsManager::PluginKeys::PRESENTER)->execCommand(data, messageID, m_clientID, *this));
    //         break;
    //     case static_cast<int>(MessageHeader::FILESYSTEM):
    //         m_socket->sendTextMessage(m_pluginsManager->getPlugin(PluginsManager::PluginKeys::FILESYSTEM)->execCommand(data, messageID, m_clientID, *this));
    //         break;
    //     default:
    //         m_socket->sendTextMessage(formatErrorMessage("Unrecognized command", messageID));
    // }
}

QJsonObject Client::parseMessage(const QString& message) {
    qDebug() << "[Network:Client:parseMessage]Called with message:s\"" + message + "\"";

    QJsonParseError* jsonParseError = new QJsonParseError();

    const auto jsonDoc = QJsonDocument::fromJson(message.toUtf8(), jsonParseError);

    if (jsonParseError->error != QJsonParseError::NoError) {
        QJsonObject errorObj;
        errorObj[ERROR_OBJECT_FIELD_NAME] = true;
        errorObj["message"] = jsonParseError->errorString();
        errorObj["code"] = jsonParseError->error;
        errorObj["offset"] = jsonParseError->offset;
        return errorObj;
    }

    delete jsonParseError;

    return jsonDoc.object();
}

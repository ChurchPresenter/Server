#include "Server.hpp"

// Qt headers
#include <QWebSocketServer>
#include <QWebSocket>

// Own headers
#include "Client.hpp"
#include <utils/GenerateRandomString.hpp>
#include <resources_manager/types/PluginsManagerType.hpp>
#include <resources_manager/types/DBManagerType.hpp>
#include <resources_manager/ResourceManagerKeys.hpp>

#include <QDebug>

Server::Server(std::shared_ptr<ResourceManager<QString>> resourceManager, QObject *parent)
    : QObject(parent)
{
    qDebug() << "[Network:Server:constructor]Called";

    m_dbManager = &resourceManager->get<ResourceManager_DBManagerType>(RESOURCEMANAGER_DATABASE)->get();
    m_pluginsManager = &resourceManager->get<ResourceManager_PluginsManagerType>(RESOURCEMANAGER_PLUGINSMANAGER)->get();
}

bool Server::run(quint16 port) {
    qDebug() << "[Network:Server:run]Called with port:" << port;

    m_server = new QWebSocketServer(QStringLiteral(""), QWebSocketServer::NonSecureMode, this);

    if (m_server->listen(QHostAddress::Any, port)) {
        connect(m_server, &QWebSocketServer::closed, this, &Server::closed);
        connect(m_server, &QWebSocketServer::newConnection, this, &Server::onNewConnection);

        connect(m_server, &QWebSocketServer::serverError, [=](QWebSocketProtocol::CloseCode closeCode){
            qDebug() << "ERROR SERVER" << closeCode;
        });

        connect(m_server, &QWebSocketServer::acceptError, [=](QAbstractSocket::SocketError socketError){
            qDebug() << "ERROR SOCKET" << socketError;
        });

        qInfo() << "Server running at" << m_server->serverUrl().toString();
        return true;
    } else {
        qCritical() << "[Server]Error binding to port " << port << ": " << m_server->errorString();
    }

    return false;
}

QPointer<Client> Server::getClient(const QString& id) {
    auto it = m_clients.find(id);

    return it != m_clients.end() ? it.value() : nullptr;
}

QList<QPointer<Client>> Server::getClients() {
    return m_clients.values();
}

void Server::onNewConnection() {
    const auto socket = m_server->nextPendingConnection();

    qDebug() << "[Network:Server:onNewConnection]New connection. Client adrress:" << socket->peerAddress();

    // Generate an unique ID for the new client
    auto id = generateRandomString(CLIENT_ID_SIZE);
    while (m_clients.find(id) != m_clients.end()) id = generateRandomString(CLIENT_ID_SIZE);

    m_clients.insert(id, new Client(socket, id, m_pluginsManager));
}

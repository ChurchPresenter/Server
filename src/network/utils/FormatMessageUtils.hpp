#ifndef NETWORK_UTILS_FORMATMESSAGEUTILS_HPP
#define NETWORK_UTILS_FORMATMESSAGEUTILS_HPP

// Qt headers
#include <QString>
#include <QJsonObject>
#include <QJsonDocument>

// Own headers
#include "../socket_data/MessageHeader.hpp"

#include <QDebug>

static QString formatMessage(const QString& code, QJsonObject& jsonObject) {
    jsonObject["code"] = QJsonValue(code);

    return QJsonDocument(jsonObject).toJson(QJsonDocument::JsonFormat::Compact);
}

static QString formatMessage(const QString& code, QJsonObject& jsonObject, const QString& id) {
    jsonObject["id"] = QJsonValue(id);
    qDebug() << "formatMessage()" << "id" << id;

    return formatMessage(code, jsonObject);
}

static QString formatErrorMessage(const QString &message) {
    QJsonObject obj;
    obj["message"] = QJsonValue(message);

    return formatMessage(MESSAGEHEADER_ERROR, obj);
}

static QString formatErrorMessage(const QString &message, const QString& id) {
    QJsonObject obj;
    obj["message"] = QJsonValue(message);

    return formatMessage(MESSAGEHEADER_ERROR, obj, id);
}

static QString formatErrorMessage(const QString &message, QJsonObject& jsonObject, const QString& id) {
    jsonObject["message"] = QJsonValue(message);
    return formatMessage(MESSAGEHEADER_ERROR, jsonObject, id);
}

static QString formatSuccessMessage(const QString& id) {
    QJsonObject obj;
    return formatMessage(MESSAGEHEADER_SUCCESS, obj, id);
}

static QString formatSuccessMessage(QJsonObject& jsonObject, const QString& id) {
    return formatMessage(MESSAGEHEADER_SUCCESS, jsonObject, id);
}

#endif // NETWORK_UTILS_FORMATMESSAGEUTILS_HPP

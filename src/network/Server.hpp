#ifndef NETWORK_SERVER_HPP
#define NETWORK_SERVER_HPP

// Qt headers
#include <QObject>
#include <QHash>
#include <QList>
#include <QPointer>

// Own headers
#include <database/DBManager.hpp>
#include <plugins_manager/PluginsManager.hpp>

class QWebSocketServer;
class Client;

/**
 * @apiGenezis
 * @class Server
 * @baseClass QObject
 * 
 * @description WebSocket server
 */
class Server : public QObject
{
    Q_OBJECT
public:
    /**
     * @apiGenezis
     * @constructor Server
     * @public
     * @explicit
     * 
     * @description The default constructor
     * 
     * @param {std::shared_ptr<ResourceManager<QString>>} resourceManager
     * @param {QObject*} [parent=nullptr]
     */
    explicit Server(std::shared_ptr<ResourceManager<QString>> resourceManager, QObject* parent = nullptr);

    /**
     * @apiGenezis
     * @function run
     * @public
     * @instanceof Server
     * 
     * @description Start the server
     * 
     * @param {quint16} port the port to bind the server to
     * 
     * @returns {bool} true if started successfully
     */
    bool run(quint16 port);

    /**
     * @apiGenezis
     * @function getClient
     * @public
     * @instanceof Server
     * 
     * @description Get a client
     * 
     * @param {const QString&} id the ID of the client
     * 
     * @returns {QPointer<Client>} the pointer to the client
     */
    QPointer<Client> getClient(const QString& id);

    /**
     * @apiGenezis
     * @function getClients
     * @public
     * @instanceof Server
     * 
     * @description Get all the client
     * 
     * @returns {QList<QPointer<Client>>} a list of pointers of all the clients
     */
    QList<QPointer<Client>> getClients();

signals:
    void closed();

private slots:
    /**
     * @apiGenezis
     * @qt_slot onNewConnection
     * @private
     * @instanceof Server
     * 
     * @description Called when a new client connect to the server
     */
    void onNewConnection();

private:
    const int CLIENT_ID_SIZE = 8;

private:
    QPointer<QWebSocketServer> m_server;
    QHash<QString, QPointer<Client>> m_clients;

    DBManager* m_dbManager;
    PluginsManager* m_pluginsManager;
};

#endif // NETWORK_SERVER_HPP

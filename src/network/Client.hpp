#ifndef CLIENT_HPP
#define CLIENT_HPP

// Qt headers
#include <QString>
#include <QObject>
#include <QPointer>
#include <QWebSocket>
#include <QJsonObject>

// Own headers
#include <database/DBManager.hpp>
#include <plugins_manager/PluginsManager.hpp>
#include <resources_manager/ResourceManager.hpp>

/**
 * @apiGenezis
 * @class Client
 * @baseClass QObject
 * 
 * @description WebSocket client
 */
class Client : public QObject
{
    Q_OBJECT
public:
    /**
     * @apiGenezis
     * @constructor Client
     * @public
     * @explicit
     * 
     * @description The default constructor
     * 
     * @param {QWebSocket*} socket pointer to the web socket of the client
     * @param {QString} clientID the identifier of the client
     * @param {PluginsManager*} pluginsManager pointer to the plugins manager
     */
    explicit Client(QWebSocket* socket, QString clientID, PluginsManager* pluginsManager);

    /**
     * @apiGenezis
     * @function getResourceManager
     * @public
     * @instanceof Client
     * 
     * @description Get a pointer to the resource manager
     * 
     * @returns {ResourceManager<QString>*} the pointer to the resource manager
     */
    ResourceManager<QString>* getResourceManager() { return &m_resourceManager; }

    /**
     * @apiGenezis
     * @function getID
     * @public
     * @instanceof Client
     * 
     * @description Get the ID of the client
     * 
     * @returns {const QString} the ID of the client
     */
    const QString getID() const { return m_clientID; }

signals:

private slots:
    /**
     * @apiGenezis
     * @qt_slot textMessageReceived
     * @private
     * @instanceof Client
     * 
     * @description The slot is called when the client receive a text message(only text?)
     * 
     * @param {const QString&} message the message received
     */
    void textMessageReceived(const QString& message);

private:
    static const QString ERROR_OBJECT_FIELD_NAME;

private:
    /**
     * @apiGenezis
     * @function parseMessage
     * @private
     * @instanceof Client
     * 
     * @description Parse the message from the client into a JSON object
     * 
     * @param {const QString&} message the message to parse
     * 
     * @returns {QJsonObject} the JSON parse message
     */
    QJsonObject parseMessage(const QString& message);

private:
    QPointer<QWebSocket> m_socket;
    QString m_clientID;
    PluginsManager* m_pluginsManager;

    ResourceManager<QString> m_resourceManager;
};

#endif // CLIENT_HPP

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QWidget>
#include <QPointer>

#include <Server/Server.hpp>
#include <DBManager.hpp>
#include <plugins_manager/PluginsManager.hpp>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:

};

#endif // MAINWINDOW_HPP

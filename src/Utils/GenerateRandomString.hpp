#ifndef UTILS_GENERATERANDOMSTRING_HPP
#define UTILS_GENERATERANDOMSTRING_HPP

#include <QString>

static QString generateRandomString(const int len) {
    QString s;

    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s.append(alphanum[rand() % (sizeof(alphanum) - 1)]);
    }

    return s;
}

#endif // UTILS_GENERATERANDOMSTRING_HPP

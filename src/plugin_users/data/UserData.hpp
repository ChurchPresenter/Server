#ifndef PLUGIN_USERS_DATA_USERDATA_HPP
#define PLUGIN_USERS_DATA_USERDATA_HPP

// Qt headers
#include <QString>
#include <QDateTime>
#include <QDebug>

// Own headers
#include <resources_manager/types/AbstractType.hpp>

struct UserData {
    QString username = "";
    QString permissions = "";

    QString  token = "";
    qint64   lastTimeUsed = 0;

    UserData() {}
    void updateLastTimeUsed() { lastTimeUsed = QDateTime().toMSecsSinceEpoch(); }

    QString toString() { return "UserData{username('" + username + "'),permissions(" + permissions + "),token(" + token + "),lastTimeUsed(" + QString::number(static_cast<qlonglong>(lastTimeUsed)) + ")}"; }
};

struct ResourceManager_UserData : ResourceManager_AbstractType {
    UserData userData;

    ResourceManager_UserData() {}

    UserData& get() {
        return userData;
    }

    static ResourceManager_UserData* create() {
        ResourceManager_UserData* type = new ResourceManager_UserData();
        return type;
    }

    static ResourceManager_UserData* createFrom(const UserData& clone) {
        ResourceManager_UserData* type = new ResourceManager_UserData();
        type->userData = clone;
        qDebug() << "[ResourceManager_UserData][createFrom]" << &type->userData << &clone;
        qDebug() << "[ResourceManager_UserData][createFrom]userData:" << type->userData.toString();
        return type;
    }
};

#endif // PLUGIN_USERS_DATA_USERDATA_HPP

#ifndef PLUGINS_USERS_USERSPLUGIN_HPP
#define PLUGINS_USERS_USERSPLUGIN_HPP

// C++ headers
#include <memory>

// Own headers
#include <plugins_manager/Plugin.hpp>

class Client;

namespace plugin_users {
    class UsersPlugin : public Plugin
    {
    public:
        UsersPlugin(std::shared_ptr<ResourceManager<QString>> resourceManager);
        virtual ~UsersPlugin() = default;

        QString execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client);

        /**
         * @description Check if a user is logged. If is logged then reset the idle timer
         * 
         * @param {Client} client the client to check if i logged
         * 
         * @returns {bool} true if the user is logged
         */
        bool checkUserLogged(Client& client);

    private:
        static const qlonglong USER_IDLE_MAX_TIME = 3 * 60 * 60 * 1000;
        static const int       USER_TOKEN_LENGTH = 10;
        static const QString   USER_RESOURCEMANAGER_USERDATA_KEY;
        
    private:
        QString actionLogin(QJsonObject& data, const QString& id, Client& client);
        QString actionCheckToken(QJsonObject& data, const QString& id, Client& client);
    };
}

#endif // PLUGINS_USERS_USERSPLUGIN_HPP

#ifndef PLUGIN_USERS_DATABASE_TABLES_USERS_HPP
#define PLUGIN_USERS_DATABASE_TABLES_USERS_HPP

// Qt headers
#include <QSqlQuery>

// Own headers
#include <database/DBManager.hpp>

namespace plugin_users::db {
    static const QString UsersCollectionName = "users";

    struct User {
        qlonglong id;
        QString username = "";
        QString password = "";
        QString permissions = "";

        QString toString() { return "DB::User{id(" + QString::number(id) + "),username(" + username + "),password(" + password + "),permissions(" + permissions + ")}"; }
    };

    bool initializeUsersTable(DBManager* dbManager);
    User getUser(DBManager* dbManager, const QString& username);
    User getUserByToken(DBManager* dbManager, const QString &token);
    void setUserToken(DBManager* dbManager, qlonglong userID, const QString& token, qlonglong expireTime);
}

#endif // PLUGIN_USERS_DATABASE_TABLES_USERS_HPP
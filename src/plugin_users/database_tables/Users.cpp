#include "Users.hpp"

// Qt headers
#include <QSqlQuery>
#include <QVariant>

#include <QDebug>

namespace plugin_users::db {
    bool initializeUsersTable(DBManager* dbManager) {
        if (!dbManager->getDB()->tables().contains(UsersCollectionName)) {
            if (!dbManager->getDB()->transaction()) {
                return false;
            }

            dbManager->getDB()->exec("CREATE TABLE `" + UsersCollectionName + "` (ID INTEGER PRIMARY KEY AUTOINCREMENT, `username` TEXT, `password` TEXT, `permissions` TEXT)");
            dbManager->getDB()->exec("CREATE UNIQUE INDEX IF NOT EXISTS UsernameUnique ON `" + UsersCollectionName + "`(`username`)");
            dbManager->getDB()->exec("INSERT INTO `" + UsersCollectionName + "`(`username`, `password`, `permissions`) VALUES ('admin', 'admin1234', 'users')");

            return dbManager->getDB()->commit();
        }

        return true;
    }

    User getUser(DBManager* dbManager, const QString& username) {
        User user;

        try {
            QSqlQuery query(*dbManager->getDB());
            query.prepare("SELECT `ID`, `password`, `permissions` FROM `users` WHERE username=:username");

            query.bindValue(":username", QVariant(username));
            query.exec();

            qDebug() << "[UsersDBUtils][getUser]Query:" << query.lastQuery();

            if (query.next()) {
                qDebug() << "[UsersDBUtils][getUser]Found user '" + username + "'";
                user.id          = query.value(0).toLongLong();
                user.username    = username;
                user.password    = query.value(1).toString();
                user.permissions = query.value(2).toString();
            }
        } catch (const std::exception& e) {
            dbManager->addError(QString(e.what()));
            qDebug() << "Error" << e.what();
        }

        return user;
    }

    User getUserByToken(DBManager* dbManager, const QString &token) {
        User user;

        try {
            QSqlQuery query(*dbManager->getDB());
            query.prepare("SELECT `userID` from `tokens` WHERE `token`=:token");

            query.bindValue(":token", token);
            query.exec();

            qlonglong userID;
            if (query.next()) {
                userID = query.value(0).toLongLong();
                user.id = userID;
            } else {
                throw std::runtime_error("The token was not found");
            }

            query.finish();

            query.prepare("SELECT `username`, `password`, `permissions` FROM `users` WHERE `ID`=:userID");

            query.bindValue(":userID", userID);
            query.exec();

            if (query.next()) {
                user.username = query.value(0).toString();
                user.password = query.value(1).toString();
                user.permissions = query.value(2).toString();
            }
        } catch (const std::exception& e) {
            dbManager->addError(QString(e.what()));
            qDebug() << "Error" << e.what();
        }

        return user;
    }

    void setUserToken(DBManager* dbManager, qlonglong userID, const QString& token, qlonglong expireTime) {
        qDebug() << "[DBManager]setUserToken()" << "'userID':" << userID << "'token':" << token << "'expireTime':" << expireTime;
        try {
            QSqlQuery query(*dbManager->getDB());
            query.prepare("REPLACE INTO `tokens`(`userID`,`token`,`expireTime`) VALUES (:userID,:token,:expireTime)");

            query.bindValue(":userID", userID);
            query.bindValue(":token", token);
            query.bindValue(":expireTime", expireTime);

            query.exec();
        } catch (const std::exception& e) {
            dbManager->addError(QString(e.what()));
            qDebug() << "Error" << e.what();
        }
    }
}
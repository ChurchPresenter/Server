#ifndef PLUGIN_USERS_SOCKET_DATA_MESSAGEACTION_HPP
#define PLUGIN_USERS_SOCKET_DATA_MESSAGEACTION_HPP

#include <QString>

namespace UsersPluginNS {
    static const QString MESSAGEACTION_LOGIN = "login";
    static const QString MESSAGEACTION_CHECK_TOKEN = "check_token";
}

#endif // PLUGIN_USERS_SOCKET_DATA_MESSAGEACTION_HPP

#include "UsersPlugin.hpp"

// Qt headers
#include <QDateTime>

// Own headers
#include "socket_data/MessageActions.hpp"
#include "data/UserData.hpp"
#include "database_tables/Users.hpp"
#include <database/DBManager.hpp>
#include <network/utils/FormatMessageUtils.hpp>
#include <network/Server.hpp>
#include <network/Client.hpp>
#include <utils/GenerateRandomString.hpp>
#include <resources_manager/ResourceManager.hpp>
#include <resources_manager/ResourceManagerKeys.hpp>
#include <resources_manager/types/DBManagerType.hpp>

namespace plugin_users {

const QString UsersPlugin::USER_RESOURCEMANAGER_USERDATA_KEY = "userData";

UsersPlugin::UsersPlugin(std::shared_ptr<ResourceManager<QString>> resourceManager)
    : Plugin(resourceManager)
{
    // TODO
    // m_resourceManager->get<ResourceManager_DBManagerType>(RESOURCEMANAGER_DATABASE)->get()

    // if (tables.contains(DB::UsersCollectionName)) DB::initializeUsersTable(this);
}

bool UsersPlugin::checkUserLogged(Client& client) {
    qDebug() << "[Plugins:Users:UsersPlugin:checkUserLogged]Called clientID='" + client.getID() + "'";

    try {
        // Get the user data from the user resource manager (which is a resource manager created for each user)
        auto userDataHLD = client.getResourceManager()->get<ResourceManager_UserData>(USER_RESOURCEMANAGER_USERDATA_KEY);

        qDebug() << "[Plugins:Users:UsersPlugin:checkUserLogged]userData = " << userDataHLD->get().toString();

        if (userDataHLD->get().lastTimeUsed >= QDateTime::currentMSecsSinceEpoch() - USER_IDLE_MAX_TIME) return false;
        userDataHLD->get().updateLastTimeUsed();

//        client.getResourceManager()->set(USER_RESOURCEMANAGER_USERDATA_KEY, userDataHLD);
    } catch (const std::exception& e) {
        qDebug() << "[Plugins:Users:UsersPlugin:checkUserLogged]Error:" << e.what();
        return false;
    }

    return true;
}

QString UsersPlugin::execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client) {
    qDebug() << "[Plugins:Users:UsersPlugin:execCommand]" << "clientID('" + clientID + "')" << "messageID('" + messageID + "')" << "message='" << message << "'";
    const QString action = message["action"].toString();

    if      (action == UsersPluginNS::MESSAGEACTION_LOGIN)       return actionLogin(message, messageID, client);
    else if (action == UsersPluginNS::MESSAGEACTION_CHECK_TOKEN) return actionCheckToken(message, messageID, client);
    else                                                         return formatErrorMessage("Unrecognized action", messageID);
}

QString UsersPlugin::actionLogin(QJsonObject& data, const QString& id, Client& client) {
    qDebug() << "[Plugins:Users:UsersPlugin:actionLogin][clientID:s" + id + "]Called with data:j\"" + QJsonDocument(data).toJson(QJsonDocument::Compact) + "\"";
    auto keys = data.keys();

    qDebug() << "[Plugins:Users:UsersPlugin:actionLogin]username='" + data["username"].toString() + "'";

    if (!keys.contains("username") || !keys.contains("password")) {
        qDebug() << "[Plugins:Users:UsersPlugin:actionLogin]'username' or 'password' not sent" << keys;
        return formatErrorMessage("Wrong command", id);
    }

    auto& dbManager = m_resourceManager->get<ResourceManager_DBManagerType>(RESOURCEMANAGER_DATABASE)->get();

    auto userStruct = plugin_users::db::getUser(&dbManager, data["username"].toString());

    qDebug() << "[Plugins:Users:UsersPlugin:actionLogin]userStruct='" << userStruct.toString() << "'";

    if (!userStruct.username.isEmpty() && userStruct.password == data["password"].toString()) {
        UserData userData;
        userData.username    = userStruct.username;
        userData.permissions = userStruct.permissions;
        userData.token       = generateRandomString(USER_TOKEN_LENGTH);
        userData.updateLastTimeUsed();

        qDebug() << "[Plugins:Users:UsersPlugin:actionLogin]userData='" << userData.toString() << "'";

        client.getResourceManager()->set(USER_RESOURCEMANAGER_USERDATA_KEY, ResourceManager_UserData::createFrom(userData));

        // Save the token
        plugin_users::db::setUserToken(&dbManager, userStruct.id, userData.token, QDateTime::currentMSecsSinceEpoch() + USER_IDLE_MAX_TIME);

        QJsonObject obj;
        obj["permissions"] = QJsonValue(userStruct.permissions);
        obj["token"]       = userData.token;
        return formatSuccessMessage(obj, id);
    } else {
        return formatErrorMessage("Wrong password or username", id);
    }
}

QString UsersPlugin::actionCheckToken(QJsonObject& data, const QString& id, Client& client) {
    qDebug() << "[Plugins:Users:UsersPlugin:actionCheckToken]Entered";
    auto keys = data.keys();

    if (!keys.contains("token")) {
        return formatErrorMessage("Wrong command", id);
    }

    const QString token = data["token"].toString();

    auto& dbManager = m_resourceManager->get<ResourceManager_DBManagerType>(RESOURCEMANAGER_DATABASE)->get();

    db::User userStruct;
    try {
        userStruct = plugin_users::db::getUserByToken(&dbManager, token);
    } catch (const std::exception&) {
        formatErrorMessage("Token is not valid", id);
    }

    qDebug() << "[Plugins:Users:UsersPlugin:actionCheckToken]userStruct='" << userStruct.toString() << "'";

    // Update the token expire time
    plugin_users::db::setUserToken(&dbManager, userStruct.id, token, QDateTime::currentMSecsSinceEpoch() + USER_IDLE_MAX_TIME);

    UserData userData;
    userData.username    = userStruct.username;
    userData.permissions = userStruct.permissions;
    userData.token       = token;
    userData.updateLastTimeUsed();

    qDebug() << "[Plugins:Users:UsersPlugin:actionCheckToken]userData='" << userData.toString() << "'";

    client.getResourceManager()->set(USER_RESOURCEMANAGER_USERDATA_KEY, ResourceManager_UserData::createFrom(userData));

    QJsonObject obj;
    obj["permissions"] = QJsonValue(userStruct.permissions);
    obj["username"]    = QJsonValue(userStruct.username);
    return formatSuccessMessage(obj, id);
}

}
#include "Plugin.hpp"

// Own headers
#include "PluginsManager.hpp"

Plugin::Plugin(std::shared_ptr<ResourceManager<QString>> resourceManager)
    : m_resourceManager { resourceManager }
{}

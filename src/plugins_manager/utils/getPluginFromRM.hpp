#ifndef PLUGINS_MANAGER_UTILS_GETPLUGINFROMRM_HPP
#define PLUGINS_MANAGER_UTILS_GETPLUGINFROMRM_HPP

// Qt headers
#include <QString>
#include <memory>

// Own headers
#include <resources_manager/ResourceManager.hpp>
#include <resources_manager/ResourceManagerKeys.hpp>
#include <resources_manager/types/PluginsManagerType.hpp>
#include <plugins_manager/PluginsManager.hpp>

template <typename T>
static std::shared_ptr<T> getPluginFromRM(PluginsManager::PluginKeys pluginType, ResourceManager<QString>& rm) {
    return std::static_pointer_cast<T>(
        rm.get<ResourceManager_PluginsManagerType>(RESOURCEMANAGER_PLUGINSMANAGER)->get().getPlugin(pluginType)
    );
}

#define getUsersPluginFromRM() getPluginFromRM<plugin_users::UsersPlugin>(PluginsManager::PluginKeys::USERS, *m_resourceManager);

#endif // PLUGINS_MANAGER_UTILS_GETPLUGINFROMRM_HPP

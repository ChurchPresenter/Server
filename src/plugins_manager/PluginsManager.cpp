#include "PluginsManager.hpp"

// Own headers
#include <plugin_presenter/PresenterPlugin.hpp>
#include <plugin_filesystem/FilesystemPlugin.hpp>
#include <plugin_users/UsersPlugin.hpp>

PluginsManager::PluginsManager(std::shared_ptr<ResourceManager<QString>> resourceManager)
    : m_resourceManager { resourceManager }
{}

void PluginsManager::addPlugin(uint code) {
    qDebug() << "[Plugins:PluginsManager:addPlugin]Called with code:i\"" + QString::number(code) + "\"";

    std::shared_ptr<Plugin> _plugin;

    switch (code) {
        case static_cast<int>(PluginKeys::PRESENTER): { _plugin = std::make_shared<PresenterPlugin>(m_resourceManager); break; }
        case static_cast<int>(PluginKeys::FILESYSTEM): { _plugin = std::make_shared<FilesystemPlugin>(m_resourceManager); break; }
        case static_cast<int>(PluginKeys::USERS): { _plugin = std::make_shared<plugin_users::UsersPlugin>(m_resourceManager); break; }
        default: throw;
    }

    m_plugins.insert({ code, _plugin });
}

std::shared_ptr<Plugin> PluginsManager::getPlugin(PluginKeys key) {
    qDebug() << "[Plugins:PluginsManager:addPlugin]Called with key:i\"" + QString::number(static_cast<int>(key)) + "\"";

    // int size = 0;
    // for (auto kv : m_plugins) { ++size; qDebug() << "WW" << kv.first << kv.second.get(); }
    // qDebug() << "SIZE" << size;
    
    return m_plugins[static_cast<int>(key)];
}
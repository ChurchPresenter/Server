#ifndef PLUGINS_MANAGER_PLUGINSMANAGER_HPP
#define PLUGINS_MANAGER_PLUGINSMANAGER_HPP

// C++ headers
#include <unordered_map>
#include <memory>
#include <functional>

// Own headers
#include <resources_manager/ResourceManager.hpp>

#include <QDebug>

class Plugin;

class PluginsManager {
public:
    enum class PluginKeys : int {
        PRESENTER = 0,
        FILESYSTEM = 1,
        USERS,

        TOTAL
    };

public:
    PluginsManager(std::shared_ptr<ResourceManager<QString>> resourceManager);

    void addPlugin(uint code);

    std::shared_ptr<Plugin> getPlugin(PluginKeys key);
private:
    std::shared_ptr<ResourceManager<QString>> m_resourceManager;

    std::unordered_map<int, std::shared_ptr<Plugin>> m_plugins;
};

#endif // PLUGINS_MANAGER_PLUGINSMANAGER_HPP

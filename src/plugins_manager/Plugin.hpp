#ifndef PLUGINS_MANAGER_PLUGIN_HPP
#define PLUGINS_MANAGER_PLUGIN_HPP

// Qt headers
#include <QJsonObject>
#include <QWebSocket>

// C++ headers
#include <memory>

// Own headers
#include <resources_manager\ResourceManager.hpp>

class Client;
class PluginsManager;

class Plugin {
public:
    Plugin(std::shared_ptr<ResourceManager<QString>> resourceManager);
    virtual ~Plugin() = default;

    virtual QString execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client) = 0;

protected:
    std::shared_ptr<ResourceManager<QString>> m_resourceManager;
};

#endif // PLUGINS_MANAGER_PLUGIN_HPP
#include "DBManager.hpp"

// Qt headers
#include <QSqlQuery>

#include <QDebug>

DBManager::DBManager()
{
}

bool DBManager::connect(const QString& databasePath) {
    try {
        m_db->setDatabaseName(databasePath);
        return m_db->open();
    } catch (const std::exception& e) {
        m_errors.append(QString(e.what()));
        qDebug() << "Error" << e.what();

        return false;
    }
}

bool DBManager::existsTable(const QString& tableName) const {
    return m_db->tables().contains(tableName);
}

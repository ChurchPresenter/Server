#ifndef DATABASE_DBMANAGER_HPP
#define DATABASE_DBMANAGER_HPP

// Qt headers
#include <QString>
#include <QStringList>
#include <QSqlDatabase>

// C++ headers
#include <memory>

/**
 * @apiGenezis
 * @class DBManager
 * 
 * @description A database manager class
 */
class DBManager
{
public:
    /**
     * @apiGenezis
     * @constructor DBManager
     * @public
     * 
     * @description The default constructor
     */
    DBManager();

    /**
     * @apiGenezis
     * @function connect
     * @public
     * @instanceof DBManager 
     * 
     * @description Connect to the database
     * 
     * @param {const QString&} databasePath the path of the SQLite database
     * 
     * @returns {bool} true if the connection to the given database was made
     */
    bool connect(const QString& databasePath);

    /**
     * @apiGenezis
     * @function existsTable
     * @public
     * @instanceof DBManager
     * 
     * @description Check if a table exists in the database
     * 
     * @param {const QString&} tableName the name of the table to check if exists
     * 
     * @returns {bool} true if the table exists
     */
    bool existsTable(const QString& tableName) const;

    /**
     * @apiGenezis
     * @function getDB
     * @public
     * @instanceof DBManager
     * 
     * @description Get an const instance to the internal database object
     * 
     * @returns {const std::shared_ptr<QSqlDatabase>} the instance to the database instance
     */
    const std::shared_ptr<QSqlDatabase> getDB() const { return m_db; }

    const QStringList& getErrors() const { return m_errors; }
    void addError(const QString& error) { m_errors.append(error); }
private:
    std::shared_ptr<QSqlDatabase> m_db = std::shared_ptr<QSqlDatabase>(new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE")));

    QStringList m_errors;
};

#endif // DATABASE_DBMANAGER_HPP

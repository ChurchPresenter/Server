#ifndef PLUGIN_FILESYSTEM_FILESYSTEMPLUGIN_HPP
#define PLUGIN_FILESYSTEM_FILESYSTEMPLUGIN_HPP

// Qt headers
#include <QStringList>

// Own headers
#include <plugins_manager/Plugin.hpp>

class FilesystemPlugin : public Plugin
{
public:
    FilesystemPlugin(std::shared_ptr<ResourceManager<QString>> resourceManager);
    virtual ~FilesystemPlugin() = default;

    QString execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client);

private:
    const QString SETTINGS_FILENAME = "filesystem.json";
    const QStringList ACCEPTED_FILES_EXTENSIONS = QStringList() << "xalps";
    const QString SEPARATOR = "/";

private:
    QString actionGetFiles(QJsonObject& data, const QString& id);

    QJsonArray getPathEntries(const QString& path);

    void readSettings(QJsonObject* data);

private:
    QStringList m_dataPaths;
    QStringList m_dataPathsDirNames;
};

#endif // PLUGIN_FILESYSTEM_FILESYSTEMPLUGIN_HPP

#include "FilesystemPlugin.hpp"

#include <QFile>
#include <QDir>

#include <QJsonArray>
#include <QJsonDocument>

#include "socket_data/MessageAction.hpp"

#include <network/utils/FormatMessageUtils.hpp>

#include <QDebug>

FilesystemPlugin::FilesystemPlugin(std::shared_ptr<ResourceManager<QString>> resourceManager)
    : Plugin(resourceManager)
{
    readSettings(nullptr);
}

QString FilesystemPlugin::execCommand(QJsonObject& message, const QString& messageID, const QString& clientID, Client& client) {
    // Login

    int action = message["action"].toInt(-1);

    if (action == FilesystemPluginNS::MESSAGEACTION_GET_FILES) return actionGetFiles(message, messageID);
    else                                                     return formatErrorMessage("Unrecognized action", messageID);
}

QString FilesystemPlugin::actionGetFiles(QJsonObject& data, const QString& id) {
    auto rootDir = data["root"].toString();

    QJsonArray entries;

    if (rootDir == "") {
        for (int i=0; i < m_dataPaths.size(); ++i) {
            QJsonObject obj;
            obj["name"] = QDir(m_dataPaths[i]).dirName();
            obj["dir"] = true;

            entries.append(obj);
        }
    } else {
        auto pathParts = rootDir.split(SEPARATOR);

        qDebug() << "pathParts" << pathParts;

        auto dataPathIndex = m_dataPathsDirNames.indexOf(pathParts.first());
        pathParts.pop_front();

        qDebug() << "dataPathIndex" << dataPathIndex;

        if (dataPathIndex == -1) {
            return formatErrorMessage("Not found the path", id);
        }

        qDebug() << pathParts.join(SEPARATOR) << m_dataPaths[dataPathIndex];
        const QString subDataPath = m_dataPaths[dataPathIndex] + SEPARATOR + pathParts.join(SEPARATOR);

        qDebug() << "subDataPath" << subDataPath;

        if (!QDir(subDataPath).exists()) {
            return formatErrorMessage("Not found the path", id);
        }

        entries = getPathEntries(subDataPath);
    }

    qDebug() << "entries:" << entries;

    QJsonObject result;
    result["entries"] = entries;

    return formatSuccessMessage(result, id);
}

QJsonArray FilesystemPlugin::getPathEntries(const QString& path) {
    auto infoEntries = QDir(path).entryInfoList(QDir::Files | QDir::Dirs, QDir::Name);
    QJsonArray entries;

    for (int i=2; i < infoEntries.size(); ++i) {
        const bool isDir = infoEntries[i].isDir();
        if (!isDir && !ACCEPTED_FILES_EXTENSIONS.contains(infoEntries[i].completeSuffix())) {
            continue;
        }

        QJsonObject obj;
        obj["name"] = infoEntries[i].fileName();
        if (isDir) obj["dir"] = true;

        entries.push_back(obj);
    }

    return entries;
}

void FilesystemPlugin::readSettings(QJsonObject* data) {
    if (data == nullptr) {
        QFile settingsFile(SETTINGS_FILENAME);

        if (!settingsFile.exists()) {
            data = new QJsonObject();

            auto appDir = QDir::current();
            appDir.mkdir("data");

            QJsonArray dataPathJSON;

            // Temporary
            dataPathJSON.append(QJsonValue(QDir::current().absolutePath() + SEPARATOR + "data"));

            data->operator[]("dataPaths") = dataPathJSON;

            QFile settingsFile(SETTINGS_FILENAME);
            settingsFile.open(QIODevice::WriteOnly);
            settingsFile.write(QJsonDocument(*data).toJson(QJsonDocument::Compact));
        } else{
            settingsFile.open(QIODevice::ReadOnly);

            data = new QJsonObject(QJsonDocument::fromJson(settingsFile.readAll()).object());
        }
    }

    auto dataPathJSON = data->value("dataPaths").toArray();
    for (int i=0; i < dataPathJSON.size(); ++i) {
        m_dataPaths.append(dataPathJSON[i].toString());
        m_dataPathsDirNames.append(QDir(m_dataPaths[i]).dirName());
    }
}

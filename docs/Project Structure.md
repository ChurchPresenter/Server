## Project Structure

- Database
  - Collections
- GUI
- Network
- Plugins
  - Filesystem
  - Presenter
  - Users
- ResourceManager
  - Types
- Server
- Utils
